package com.twuc.webApp;

import com.twuc.webApp.contract.BagSize;
import com.twuc.webApp.contract.SlotSize;
import com.twuc.webApp.entity.Bag;
import com.twuc.webApp.entity.Storage;
import com.twuc.webApp.entity.Ticket;
import com.twuc.webApp.exception.CantSaveException;
import com.twuc.webApp.exception.InvalidTicketException;
import com.twuc.webApp.exception.StorageFullException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {

    private static Storage createInfiniteCapacityStorage() {
        return new Storage(100);
    }

    @Test
    void should_get_ticket_when_saving_a_bag() throws RuntimeException {
        Storage storage = createInfiniteCapacityStorage();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_ticket_when_saving_nothing() throws RuntimeException {
        Storage storage = createInfiniteCapacityStorage();
        Bag nothing = null;

        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag_when_retrieve_using_valid_ticket() {
        Storage storage = createInfiniteCapacityStorage();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        Bag actualBag = storage.retrieve(ticket);
        assertSame(bag, actualBag);
    }

    @Test
    void should_get_saved_bag_when_using_valid_ticket() throws RuntimeException {

        Bag otherBag = new Bag();
        Bag expectedBag = new Bag();

        Storage storage = createInfiniteCapacityStorage();
        storage.save(otherBag);
        Ticket ticket = storage.save(expectedBag);

        Bag actualBag = storage.retrieve(ticket);
        assertSame(expectedBag, actualBag);
        assertNotSame(otherBag, actualBag);
    }

    @Test
    void should_return_error_message_when_given_invalid_ticket() throws RuntimeException {

        Storage storage = createInfiniteCapacityStorage();
        storage.save(new Bag());
        Ticket invalidTicket = new Ticket();

        Exception theException = assertThrows(InvalidTicketException.class, () -> storage.retrieve(invalidTicket));
        assertEquals("Invalid Ticket", theException.getMessage());
    }

    @Test
    void should_return_error_message_when_give_ticket_and_empty_storage() {

        Storage emptyStorage = createInfiniteCapacityStorage();
        Ticket invalidTicket = new Ticket();

        Exception theException = assertThrows(InvalidTicketException.class, () -> emptyStorage.retrieve(invalidTicket));
        assertEquals("Invalid Ticket", theException.getMessage());
    }

    @Test
    void should_return_nothing_when_using_valid_ticket() throws RuntimeException {
        Storage storage = createInfiniteCapacityStorage();
        Bag nothing = null;

        Ticket validTicket = storage.save(nothing);
        Bag actualBag = storage.retrieve(validTicket);
        assertNull(actualBag);
    }

    @Test
    void should_return_error_message_when_using_ticket_twice() throws RuntimeException {

        Storage storage = createInfiniteCapacityStorage();
        Ticket ticket = storage.save(new Bag());

        Exception theException = assertThrows(InvalidTicketException.class, () -> {
            storage.retrieve(ticket);
            storage.retrieve(ticket);
        });
        assertEquals("Invalid Ticket", theException.getMessage());
    }


    //

    @Test
    void should_return_ticket_when_saving_the_bag() throws RuntimeException {
        Storage storage = new Storage(2);
        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
    }

    @Test
    void should_return_message_when_saving_the_bag() {
        Storage storage = new Storage(2);

        Exception theException = assertThrows(StorageFullException.class, () -> {
            storage.save(new Bag());
            storage.save(new Bag());
            storage.save(new Bag());
        });
        assertEquals("Insufficient Capacity", theException.getMessage());
    }

    @Test
    void should_return_ticket_and_message_when_saving_two_bag() throws RuntimeException {
        Storage storage = new Storage(2);
        storage.save(new Bag());
        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
        assertThrows(StorageFullException.class, () -> storage.save(new Bag()), "Insufficient Capacity");
    }

    @Test
    void should_retrieve_when_saving_bag_and_retrieve_bag() throws RuntimeException {
        Storage storage = new Storage(2);
        storage.save(new Bag());
        Ticket ticket = storage.save(new Bag());
        Bag bag = storage.retrieve(ticket);
        assertNotNull(bag);
        Ticket actualTicket = storage.save(new Bag());
        assertNotNull(actualTicket);
    }

    @Test
    void should_return_ticket_when_saving_big_bag_to_big_slot() {
        Storage storage=new Storage(0,0,1);
        Ticket ticket=storage.save(new Bag(BagSize.BIG), SlotSize.BIG);
        Bag bag=storage.retrieve(ticket);
        assertNotNull(bag);
    }

    @Test
    void should_return_message_when_saving_big_bag_to_small_slot() {
        Storage storage=new Storage(2,2,2);
        assertThrows(CantSaveException.class,()->storage.save(new Bag(BagSize.BIG), SlotSize.SMALL),"Cannot save your bag: big small");
    }

    @Test
    void should_return_message_when_saving_big_bag_to_big_slot() {
        Storage storage=new Storage(1,1,0);
        assertThrows(StorageFullException.class,()->storage.save(new Bag(BagSize.BIG), SlotSize.BIG),"Insufficient capacity");
    }

    @Test
    void should_return_ticket_when_saving_medium_bag_to_medium_bag() {
        Storage storage=new Storage(0,1,0);
        Bag exceptedBag = new Bag(BagSize.MEDIUM);
        Ticket ticket = storage.save(exceptedBag, SlotSize.MEDIUM);
        assertNotNull(ticket);
        Bag actualBag = storage.retrieve(ticket);
        assertSame(exceptedBag,actualBag);
    }
}