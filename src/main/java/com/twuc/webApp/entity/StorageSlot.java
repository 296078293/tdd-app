package com.twuc.webApp.entity;

import com.twuc.webApp.contract.SlotSize;
import com.twuc.webApp.exception.InvalidTicketException;
import com.twuc.webApp.exception.StorageFullException;

import java.util.HashMap;
import java.util.Map;

public class StorageSlot {
    private Map<Ticket, Bag> storage;
    private SlotSize slotSize;
    private int usedSlot;
    private final int capacity;

    public StorageSlot(int capacity) {
        this.capacity = capacity;
    }

    public StorageSlot( int capacity,SlotSize slotSize) {
        this.storage = new HashMap<>();
        this.slotSize=slotSize;
        this.usedSlot=0;
        this.capacity = capacity;
    }

    Ticket save(Bag bag){
        if (usedSlot == capacity) {
            throw new StorageFullException();
        }
        Ticket ticket = new Ticket();
        storage.put(ticket, bag);
        usedSlot++;
        return ticket;
    }

    Bag retrieve(Ticket ticket) throws InvalidTicketException {
        if (!storage.containsKey(ticket)) {
            throw new InvalidTicketException();
        }
        Bag bag = storage.get(ticket);
        storage.remove(ticket);
        usedSlot--;
        return bag;
    }
}
