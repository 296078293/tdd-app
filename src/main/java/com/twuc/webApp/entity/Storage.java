package com.twuc.webApp.entity;


import com.twuc.webApp.contract.SlotSize;
import com.twuc.webApp.exception.CantSaveException;
import com.twuc.webApp.exception.InvalidTicketException;
import com.twuc.webApp.exception.StorageFullException;

import java.util.HashMap;
import java.util.Map;

public class Storage {

    private Map<SlotSize, StorageSlot> storage;
    private Map<Ticket, StorageSlot> ticketMap;

    private StorageSlot smallSlot;
    private StorageSlot mediumSlot;
    private StorageSlot bigSlot;

    public Storage(int smallCapacity) {
        this(smallCapacity, 0, 0);
    }

    public Storage(int smallCapacity, int mediumCapacity, int bigCapacity) {

        this.storage = new HashMap<>();
        this.ticketMap = new HashMap<>();
        bigSlot = new StorageSlot(bigCapacity, SlotSize.BIG);
        mediumSlot = new StorageSlot(mediumCapacity, SlotSize.MEDIUM);
        smallSlot = new StorageSlot(smallCapacity, SlotSize.SMALL);
        storage.put(SlotSize.BIG, bigSlot);
        storage.put(SlotSize.MEDIUM, mediumSlot);
        storage.put(SlotSize.SMALL, smallSlot);
    }

    public Ticket save(Bag bag) throws StorageFullException {
        return save(bag, SlotSize.SMALL);
    }

    public Ticket save(Bag bag, SlotSize slotSize) {
        if (bag != null) {
            if (!slotSize.canSave(bag.getSize())) {
                throw new CantSaveException(String.format("Cannot save your bag: %s %s", bag.getSize(), slotSize));
            }
        }
        Ticket ticket = storage.get(slotSize).save(bag);
        ticketMap.put(ticket, storage.get(slotSize));
        return ticket;
    }

    public Bag retrieve(Ticket ticket) throws InvalidTicketException {

        if (!ticketMap.containsKey(ticket)) {
            throw new InvalidTicketException();
        }
        StorageSlot storageSlot = ticketMap.get(ticket);
        Bag bag = storageSlot.retrieve(ticket);
        return bag;
    }
}
