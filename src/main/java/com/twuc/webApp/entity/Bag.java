package com.twuc.webApp.entity;

import com.twuc.webApp.contract.BagSize;
import com.twuc.webApp.contract.SlotSize;

public class Bag {

    private BagSize bagSize;

    public Bag() {
        bagSize = bagSize.SMALL;
    }

    public Bag(BagSize bagSize) {
        this.bagSize = bagSize;
    }

    public BagSize getSize() {
        return bagSize;
    }
}
