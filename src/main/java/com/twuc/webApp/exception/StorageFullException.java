package com.twuc.webApp.exception;

public class StorageFullException extends RuntimeException{

    public StorageFullException() {
        super("Insufficient Capacity");
    }

    public StorageFullException(String message) {
        super(message);
    }


}
