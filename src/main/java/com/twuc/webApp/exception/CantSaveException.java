package com.twuc.webApp.exception;

public class CantSaveException extends RuntimeException{
    public CantSaveException() {
    }

    public CantSaveException(String message) {
        super(message);
    }
}
