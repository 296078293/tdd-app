package com.twuc.webApp.exception;

public class InvalidTicketException extends RuntimeException {


    public InvalidTicketException() {
        super("Invalid Ticket");
    }

    public InvalidTicketException(String message) {
        super(message);
    }
}
