package com.twuc.webApp.contract;

public enum BagSize {
    BIG(3), MEDIUM(2), SMALL(1);

    BagSize(int code) {
        this.code = code;
    }

    private int code;

    public int getCode() {
        return code;
    }
}
