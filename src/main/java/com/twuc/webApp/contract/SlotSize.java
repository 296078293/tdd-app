package com.twuc.webApp.contract;

public enum SlotSize {
    BIG(3), MEDIUM(2), SMALL(1);

    SlotSize(int code) {
        this.code = code;
    }

    private int code;

    public boolean canSave(BagSize bagSize) {
        if (bagSize.getCode() > this.code) {
            return false;
        }
        return true;
    }

}
